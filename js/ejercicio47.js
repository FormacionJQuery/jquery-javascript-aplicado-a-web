/*
 - Cambiar la validación de los obligatorios, poniendo los identificadores en un array y usando for..of
*/

$(function () {
    $("button:submit").click(validar);

    $('#cp').keyup(dejarNumeros);
    $("#password").on("keyup", validarFuerzaPassword);
    $("#nombre, #apellidos, #direccion, #cp").on("keyup", validarControlObligatorioKeyup);
    $("#email").on("keyup", validarControlEmail);
    $("#password-repeat").on("keyup", validarControlPasswordRepeat);
});

function dejarNumeros() {
    this.value = this.value.replace(/[^0-9]/g,'');
}

function validarControlObligatorioKeyup() {
    validarControlObligatorio(this.id);
}

function validarObligatorio(valor) {
    return valor.length > 0;
}

function validarControlObligatorio(idControl) {    
    let control = $('#' + idControl);
    let esValido = validarObligatorio(control.val());
    control.next().toggleClass('d-none', esValido);

    return esValido;
}

function validarEmail(email) {
    let expresionRegular = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!expresionRegular.test(email)) {
        return false;
    }else{
        return true;
    }
}

function validarControlEmail() {
    let email = $('#email');
    let esValido = validarEmail(email.val());
    email.next().toggleClass('d-none', esValido);

    return esValido;
}

function validarFuerzaPassword() {
    let numeros = /([0-9])/;
    let letras = /([a-zA-Z])/;
    let caracteresEspeciales = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
    let contrasenyaValida = false;
    let contrasenya = $('#password').val();
    let controlMensajePassword = $('#password').next();
    controlMensajePassword.removeClass();

    if (contrasenya.length == 0) {
        controlMensajePassword.text("La Contraseña es obligatoria.");
        controlMensajePassword.addClass("text-danger");
    } else if (contrasenya.length < 6) {
        controlMensajePassword.text("Contraseña débil, debería tener al menos 6 caracteres");
        controlMensajePassword.addClass("text-danger");
    } else {
        if (contrasenya.match(numeros) && contrasenya.match(letras) && contrasenya.match(caracteresEspeciales)) {
            controlMensajePassword.addClass("text-success");
            controlMensajePassword.text("Contraseña fuerte");
            contrasenyaValida = true;
        } else {
            controlMensajePassword.addClass("text-warning");
            controlMensajePassword.text("Contraseña aceptable, sería mejor que incluyera números y carácteres especiales");
            contrasenyaValida = true;
        }
    }

    return contrasenyaValida;
}

function validarControlPasswordRepeat() {
    let passwordRepeat = $('#password-repeat');
    let esValido = passwordRepeat.val() == $('#password').val();
    passwordRepeat.next().toggleClass('d-none', esValido);

    return esValido;
}

function validar(evento) {
    let esValido = true;
    esValido = validarControlObligatorio('nombre') && esValido;
    esValido = validarControlObligatorio('apellidos') && esValido;
    esValido = validarControlEmail() && esValido;
    esValido = validarControlObligatorio('direccion') && esValido;
    esValido = validarControlObligatorio('cp') && esValido;
    esValido = validarFuerzaPassword() && esValido;
    esValido = validarControlPasswordRepeat() && esValido;

    if (!esValido) {
        alert('Corrige los campos antes de continuar');
    } else if (!$('#politica-privacidad').is(':checked')) {
        alert('Hay que aceptar la política de privacidad');
        esValido = false;
    }

    if (!esValido) {
        evento.preventDefault();
    }
}