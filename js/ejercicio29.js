/*
 - Registrar el evento click en el botón Guardar
 - Evitar que el formulario se envíe si hay algún error en el formulario
*/

function validarObligatorio(valor) {
    return valor.length > 0;
}

function validarControlObligatorio(idControl) {    
    let control = $('#' + idControl);
    let esValido = validarObligatorio(control.val());
    control.next().toggleClass('d-none', esValido);
}

function validarEmail(email) {
    let expresionRegular = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!expresionRegular.test(email)) {
        return false;
    }else{
        return true;
    }
}

function validarControlEmail() {
    let email = $('#email');
    let esValido = validarEmail(email.val());
    email.next().toggleClass('d-none', esValido);    
}

function validarControlPasswordRepeat() {
    let passwordRepeat = $('#password-repeat');
    let esValido = passwordRepeat.val() == $('#password').val();
    passwordRepeat.next().toggleClass('d-none', esValido);    
}

function validar() {
    validarControlObligatorio('nombre');
    validarControlObligatorio('apellidos');
    validarControlEmail();
    validarControlObligatorio('direccion');
    validarControlObligatorio('cp');
    validarControlObligatorio('password');
    validarControlPasswordRepeat();

    if (!$('#politica-privacidad').is(':checked')) {
        alert('Hay que aceptar la política de privacidad');
    }
}