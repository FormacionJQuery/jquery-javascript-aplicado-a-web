class Marcador {
    vidas = 0;
    contenedor;
    plantillaVida = $("<svg viewBox='0 0 512 512' style='width: 16px; color:#dc3545;'><path fill='currentColor' d='M462.3 62.6C407.5 15.9 326 24.3 275.7 76.2L256 96.5l-19.7-20.3C186.1 24.3 104.5 15.9 49.7 62.6c-62.8 53.6-66.1 149.8-9.9 207.9l193.5 199.8c12.5 12.9 32.8 12.9 45.3 0l193.5-199.8c56.3-58.1 53-154.3-9.8-207.9z'></path></svg>");

    constructor(contenedor, vidas) {
        //Asignar el contenedor a la variable contendor de Marcador para usar más adelante
        //Asignar las vidas a la variable vidas de Marcador para usar más adelante
        //Llamar al método mostrarVidas
    }

    limpiarMarcador() {
        //Eliminar todos los hijos del contenedor
    }

    mostarVidas() {
        //Llamar al método limpiarMarcador
        //Utilizando un while, por cada vida, poner una copia de plantillaVida en el contenedor
    }

    quitar() {
        //Si el número de vidas es mayour a 0
            //Quitar el color rojo al último hijo que tenga el color rojo (debería ser el que está en la posición a vidas actuales)
            //Restar una vida
        //Devolver el número de vidas restantes
    }
}