/*
 - Aplicar un evento keyup al control código postal para que elimine todo lo que no sean números (reemplazar a vacío todo lo que no sean números /[^0-9]/g)
 - Aplicar un evento keyup al control con id password para que aplique lo siguiente:
    - Elimine cualquier clase del div siguiente al control password (a partir de ahora lo llamaré divError)
    - Compruebe la largura de la contraseña, si esta es 0, poner la clase text-danger y el mensaje "La Contraseña es obligatoria." al divError
    - Compruebe la largura de la contraseña, si esta es menor a 6, poner la clase text-danger y el mensaje "Contraseña débil, debería tener al menos 6 caracteres" al divError
    - Compruebe la contraseña, aplicando la lógica que queráis (que tenga algún número, carácter especial... buscar en internet), si no lo cumple poner la clase text-warning y el mensaje "Contraseña aceptable, sería mejor que incluyera números y carácteres especiales" al divError
    - Si cumple todo lo anterior, aplicar la clase text-success y el mensaje "Contraseña fuerte" al divError
 - Modificar la función validar para que tenga en cuenta los nuevos validadores 
 - Registrar el evento keyup para los distintos controles con validación y aplicar su respectiva validaciones (salvo a password);
*/

$(function () {
    $("button:submit").click(validar);
});

function validarObligatorio(valor) {
    return valor.length > 0;
}

function validarControlObligatorio(idControl) {    
    let control = $('#' + idControl);
    let esValido = validarObligatorio(control.val());
    control.next().toggleClass('d-none', esValido);

    return esValido;
}

function validarEmail(email) {
    let expresionRegular = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!expresionRegular.test(email)) {
        return false;
    }else{
        return true;
    }
}

function validarControlEmail() {
    let email = $('#email');
    let esValido = validarEmail(email.val());
    email.next().toggleClass('d-none', esValido);

    return esValido;
}

function validarControlPasswordRepeat() {
    let passwordRepeat = $('#password-repeat');
    let esValido = passwordRepeat.val() == $('#password').val();
    passwordRepeat.next().toggleClass('d-none', esValido);

    return esValido;
}

function validar(evento) {
    let esValido = true;
    esValido &= validarControlObligatorio('nombre');
    esValido &= validarControlObligatorio('apellidos');
    esValido &= validarControlEmail();
    esValido &= validarControlObligatorio('direccion');
    esValido &= validarControlObligatorio('cp');
    esValido &= validarControlObligatorio('password');
    esValido &= validarControlPasswordRepeat();

    if (!esValido) {
        alert('Corrige los campos antes de continuar');
    } else if (!$('#politica-privacidad').is(':checked')) {
        alert('Hay que aceptar la política de privacidad');
        esValido = false;
    }

    if (!esValido) {
        evento.preventDefault();
    }
}